%global realname gettext
%global upstream etnt
%global git_tag 2bcba56069721afc7d8c58f84ce701958ec7a761
%global short_tag %(c=%{git_tag}; echo ${c:0:7})
Name:                erlang-%{realname}
Version:             2.1.0
Release:             2
BuildArch:           noarch
Summary:             Erlang internationalization library
License:             MIT
URL:                 https://github.com/%{upstream}/%{realname}
VCS:                 scm:git:https://github.com/%{upstream}/%{realname}.git
Source0:             https://github.com/%{upstream}/%{realname}/archive/%{git_tag}/%{realname}-%{version}.tar.gz
BuildRequires:       erlang-rebar
BuildRequires:       erlang-edoc
%description
Erlang internationalization library.

%prep
%autosetup -p1 -n %{realname}-%{git_tag}

%build
%{erlang_compile}

%install
%{erlang_install}

%check
%{erlang_test}

%files
%license LICENSE
%doc README
%{erlang_appdir}/

%changelog
* Thu Jul 20 2023 yaoxin <yao_xin001@hoperun.com> - 2.1.0-2
- Fix build error

* Sat Aug 29 2020 wangyue <wangyue92@huawei.com> - 2.1.0-1
- package init
